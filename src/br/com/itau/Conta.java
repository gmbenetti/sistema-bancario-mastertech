package br.com.itau;

import static br.com.itau.Constantes.*;

public class Conta {
    private String agenciaConta = "";
    private String numeroConta = "";
    private Cliente cliente = new Cliente();
    private Integer saldo  = 0;

    public Conta(Cliente cliente, String agenciaConta, String numeroConta){
        this.cliente = cliente;
        this.agenciaConta = agenciaConta;
        this.numeroConta = numeroConta;
        this.saldo = 0;
    }

    public String getAgenciaConta() {
        return agenciaConta;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void sacarSaldo(Integer valor){
        if(valor <= this.saldo){
            this.saldo -= valor;
            geraLog(SAQUE, valor,OK,"saque efetuado com sucesso");
        }else{
            geraLog(SAQUE,valor,NOK,"saldo insuficiente");
        }
    }

    public void depositar(Integer valor){
        this.saldo += valor;
        geraLog(DEPOSITO, valor, OK,"deposito efetuado com sucesso");
    }

    public Integer consultarSaldo(){
        geraLog(CONSULTA_SALDO, this.saldo,OK,"");
        return this.saldo;
    }

    public String consultaNomeCliente(Cliente cliente){
        return cliente.getNome();
    }

    private void geraLog(String operacao, Integer valorOperacao, String status, String mensagem){
        System.out.println("\n-----------------------------------------------------------------------------------------------------");
        System.out.println("|operacao:" + operacao + "|valor:" + valorOperacao + "|status:" + status + "|msg:" + mensagem + "|");
        if(operacao != CONSULTA_SALDO){
            System.out.println("|saldo disponível: " + this.saldo + "|");
        }
        System.out.println("-----------------------------------------------------------------------------------------------------\n");
    }
}
