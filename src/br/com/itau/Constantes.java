package br.com.itau;

public class Constantes {
    public static final String DEPOSITO = "deposito";
    public static final String SAQUE = "saque";
    public static final String CONSULTA_SALDO = "consulta saldo";
    public static final String OK = "OK";
    public static final String NOK = "NOK";
}
