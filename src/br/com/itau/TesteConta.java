package br.com.itau;

import br.com.itau.Cliente.*;

public class TesteConta {

    public TesteConta(){


        System.out.println("\n*******************CLIENTE 1***************************************************************************");
        Cliente cliente1 = new Cliente("Jose", "aposentado", 60);
        Conta conta1 = new Conta(cliente1, "500", "40123");
        System.out.println("nome cliente1 " + conta1.consultaNomeCliente(cliente1));
        System.out.println("profissão cliente1 " + cliente1.getProfissao());
        System.out.println("idade cliente1 " + cliente1.getIdade());
        System.out.println("Agencia cliente1 " + conta1.getAgenciaConta());
        System.out.println("Conta cliente1 " + conta1.getNumeroConta());
        conta1.depositar(100);
        conta1.sacarSaldo(30);
        System.out.println("*******************************************************************************************************");

        System.out.println("\n*******************CLIENTE 2***************************************************************************");
        Cliente cliente2 = new Cliente("Joao", "estudante", 18);
        Conta conta2 = new Conta(cliente2, "499", "89714");
        System.out.println("nome cliente2 " + conta2.consultaNomeCliente(cliente2));
        System.out.println("profissão cliente2 " + cliente2.getProfissao());
        System.out.println("idade cliente2 " + cliente2.getIdade());
        System.out.println("Agencia cliente2 " + conta2.getAgenciaConta());
        System.out.println("Conta cliente2 " + conta2.getNumeroConta());
        conta2.depositar(150);
        conta2.consultarSaldo();
        conta2.sacarSaldo(200);
        conta2.sacarSaldo(110);
        System.out.println("*******************************************************************************************************");

        System.out.println("\n*******************CLIENTE 3***************************************************************************");
        Cliente cliente3 = new Cliente("Maria", "estudante", 17);
        if(cliente3.getIdade() >= 18) {
            Conta conta3 = new Conta(cliente3, "500", "11111");
            System.out.println("nome cliente2 " + conta3.consultaNomeCliente(cliente3));
            System.out.println("profissão cliente2 " + cliente3.getProfissao());
            System.out.println("idade cliente2 " + cliente3.getIdade());
        }else{
            System.out.println("Não foi possível gerar nova conta, pois o cliente está com dados incompletos");
        }
        System.out.println("*******************************************************************************************************");
    }
}
