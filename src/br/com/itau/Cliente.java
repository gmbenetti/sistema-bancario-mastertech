package br.com.itau;

public class Cliente {
    private String nome = "";
    private String profissao = "";
    private Integer idade = 0;


    public Cliente(String nome, String profissao, Integer idade){
        if(idade >= 18) {
            this.nome = nome;
            this.profissao = profissao;
            this.idade = idade;
        }else {
            System.out.println("Não é permitido cliente com menos de 18 anos.");

            return;
        }
    }

    public Cliente(){}

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }
}
